import React from 'react'
import { connect } from 'react-redux'
import Label from './label'

const fun1 = () => {
    console.log('Klik')
}

function PersonalArea(props) {
  console.log(props)
  const {name, lastName, update, text} = props
  return (
    <form onSubmit={(e)=>{e.preventDefault()}}>
        <Label name={text} fun={fun1} />
        <div>{name}</div>
        <div>{lastName}</div>
        {/* <input type="text" name="name" placeholder="Name" value={firstName}/>
        <input type="text" name="lastname" placeholder="Last name" value={lastName}/> */}
        <button onClick={() => {update('Tomek', 'Czarny')}}>Tomek</button>
        <button onClick={() => {update('Paweł', 'Faryna')}}>Paweł</button>
        <button onClick={() => {update('Wojtek', 'Wojcik')}}>Wojtek</button>
    </form>
  );
}

const mapStateToProps = (props) => {
    return props
}

const mapDispatchToProps = (dispatch) => {
    return {
        update: (name, lastName) => {
            dispatch({type: 'ADD_NAME', payload: name})
            dispatch({type: 'ADD_LASTNAME', payload: lastName})
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PersonalArea);