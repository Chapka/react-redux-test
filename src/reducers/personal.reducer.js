const defaultState = {
    name: 'Paweł',
    lastName: 'Faryna',
    age: 0,
    sex: ''
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ADD_NAME':
          return {
            ...state,
            name: action.payload
          }
        case 'ADD_LASTNAME':
          return {
            ...state,
            lastName: action.payload
          }
        default:
          return state
      }
}