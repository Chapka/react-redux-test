import React from 'react'
import Name from './name'

export default function (props) {
    return (
        <span>Halo <Name name={props.name} fun={props.fun}/> to jest nasza labelka</span>
    )
}