import React from 'react'

export default function (props) {
    return (
        <strong onClick={props.fun}>{props.name}</strong>
    )
}